This build only supports headless mode.

INSTRUCTIONS
To start monsters-land-application in headless mode, run:
	./monsters-land-applicationHeadless_linux.sh
or run:
	./linux_Core.x86_64 -batchmode -nographics

You might have to grant execute permissions first by running:
	chmod +x monsters-land-applicationHeadless_linux.sh
	chmod +x linux_Core.x86_64
